
#include "P2P.h"

#include <opencv2//highgui//highgui.hpp>
#include <ctime>
#include <iostream>


P2P::P2P(IplImage* imagemInput) : Core((unsigned char*) imagemInput->imageData, imagemInput->height, imagemInput->widthStep) {
	data nada = {};
	parametros = nada;
}

P2P::P2P(unsigned char* imagemInput, int altura, int largura, data parametros): Core(imagemInput, altura, largura), parametros(parametros) {	}

int P2P::processaEspecifico(unsigned char* imagemOriginal, unsigned char* imagemResultante, int largura, int altura){		

	
	kP2P(imagemOriginal, imagemResultante, tamanho, parametros);
	
	return erro == cudaSuccess;
}

static void printaTempo(clock_t* start, char* texto) {
	clock_t finish = clock() - *start;
	double interval = finish / (double)CLOCKS_PER_SEC; 
	std::cout << texto <<  std::endl << " - " << interval * 1000 << "ms" << std::endl;
	*start = clock();
}

int main(int argc, char* argv[]) {

	clock_t start;
	clock_t startMaster;

	
	const char* imagemEntrada = argc == 1 ? "img.jpg" : argv[1]; 

	IplImage *imagem = cvLoadImage(imagemEntrada, CV_LOAD_IMAGE_GRAYSCALE);
	unsigned char* imageData = (unsigned char*) imagem->imageData;	

	start = startMaster = clock();

	printaTempo(&start, "Carregada imagem");

	/*finish = clock() - start;
	double interval = finish / (double)CLOCKS_PER_SEC; 
	std::cout << "Carregada imagem" << std::endl;
	printf(" tempo : %f ms \n", interval * 1000);
	start = clock();*/
	
	/*data parametros;
	parametros.imageSubtration = cvLoadImage(argv[2], CV_LOAD_IMAGE_GRAYSCALE)->imageData;	
	parametros.limiar = 40;*/

	P2P* p2p = new P2P(imagem);

	printaTempo(&start, "Criada classe");

	std::cout << "Fui pro CUDA" << std::endl;
		
	p2p->processa();

	printaTempo(&start, "Voltei do CUDA");
	
	printaTempo(&startMaster, "Tempo total");

	//p2p->mostraResultado(imagem);

	return 0;
}
 