#include <cuda_runtime.h>
#include "device_launch_parameters.h"
#include "P2P.h"
#include <stdio.h>
#include <iostream>
#include "math.h"
#include <cuda_profiler_api.h>
#include <ctime>

#define CHK_ERROR if (erro != cudaSuccess) goto Error;

__device__
	data param;

__device__ 
unsigned char subtraction(char in, data param, int i) {

	int result = in-param.imageSubtration[i] - param.limiar;
	result = result > 0 ? result : result * -1;
	return result;
}

__device__
static unsigned char polarizador(unsigned char pixel){
	return pixel > 127 ? 255 : 0;
}

__global__ void P2P_kernel(unsigned char* imagem, int tamanho){//, data param){
    int i = blockIdx.x * blockDim.x + threadIdx.x;
	    if(i < tamanho)
        	imagem[i] = polarizador(imagem[i]);//subtraction (imagem[i], param, i);
}

static void printaTempo(clock_t* start, char* texto) {
	clock_t finish = clock() - *start;
	double interval = finish / (double)CLOCKS_PER_SEC; 
	std::cout << texto <<  std::endl << " - " << interval * 1000 << "ms" << std::endl;
	*start = clock();
}

extern "C" void kP2P(unsigned char* imagem, unsigned char* imageResult, int tamanho, data parametros){

	clock_t start = clock();

	cudaDeviceProp deviceProp;
	cudaError_t erro;
	unsigned char* d_image;
	//unsigned char* d_imageResult;

	//cudaProfilerStart();

	cudaEvent_t startCuda, stop;
	cudaEventCreate(&startCuda);
	cudaEventCreate(&stop);

	cudaEventRecord(startCuda);

	erro = cudaMalloc((void**)&d_image,sizeof(unsigned char)*tamanho); CHK_ERROR
	//erro = cudaMalloc((void**)&d_imageResult,sizeof(unsigned char)*tamanho); CHK_ERROR
	
	printaTempo(&start, "cudaMallocs");
	//erro = cudaMalloc((void**)&param.imageSubtration,sizeof(parametros.imageSubtration)); CHK_ERROR	

	erro = cudaMemcpy(d_image,imagem,tamanho*sizeof(unsigned char),cudaMemcpyHostToDevice);
	//erro = cudaMemcpy(param.imageSubtration,parametros.imageSubtration,sizeof(parametros.imageSubtration),cudaMemcpyHostToDevice); CHK_ERROR

	printaTempo(&start, "cudaMemcpy");

	cudaGetDeviceProperties(&deviceProp, 0);
	int blockSize = deviceProp.maxThreadsPerBlock;
	int nBlocks = tamanho/blockSize + (tamanho%blockSize == 0 ? 0:1);
	
	//if(blockSize > deviceProp.max)
	P2P_kernel<<<nBlocks, blockSize>>>(d_image,tamanho);//, param);	

	erro = cudaGetLastError(); CHK_ERROR

	//Espera por GPU terminar o trabalho
	erro = cudaDeviceSynchronize(); CHK_ERROR

	printaTempo(&start, "Kernel executado");

	erro = cudaMemcpy(imageResult,d_image,tamanho*sizeof(unsigned char),cudaMemcpyDeviceToHost); CHK_ERROR
	
	printaTempo(&start, "Dados copiados de volta");

	

	
	//cudaProfilerStop();
	//cudaDeviceReset();

	goto Free;

Error:	
	std::cerr << "Error on CUDA: " << cudaGetErrorString(erro);

Free:
	cudaFree(d_image);
	//cudaFree(d_imageResult);

	cudaEventRecord(stop);
	cudaEventSynchronize(stop);
	float milliseconds = 0;
	cudaEventElapsedTime(&milliseconds, startCuda, stop);
	std::cout << "cudaEventElapsedTime in milliseconds : " << milliseconds << std::endl;
}
